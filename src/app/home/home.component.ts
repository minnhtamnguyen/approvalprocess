import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserServiceService } from '../user-service.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateProjectComponent } from '../create-project/create-project.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  projects: Observable<any[]>;
  currentUser: any;

  constructor(
    private apollo: Apollo,
    private userService: UserServiceService,
    public dialog: MatDialog
    ) { }
  ngOnInit() {
    this.refresh()
  }


  refresh(){
    this.projects = this.apollo.subscribe<any>({
      query: gql`
      subscription{
        allProjects{
          id
          name
          status
          state
          projectCode
        }
      }
      `
    })
      .pipe(
        map(result => result.data['allProjects'])
      );
      this.projects.subscribe()
  }

  getUser(){
    this.userService.getUser()
  }

  createProjectDialog(): void {
    const dialogRef = this.dialog.open(CreateProjectComponent, {
      width: '30%',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  projectId: String
  constructor(public route: ActivatedRoute) { }
  
  ngOnInit() {
    this.route.params.subscribe(p => {
      this.projectId = p['projectId'];
    })
  }

}

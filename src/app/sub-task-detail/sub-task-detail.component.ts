import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { map } from 'rxjs/operators';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sub-task-detail',
  templateUrl: './sub-task-detail.component.html',
  styleUrls: ['./sub-task-detail.component.css']
})
export class SubTaskDetailComponent implements OnInit {
  taskCodeForSubTask: any
  taskSub: Observable<any>;
  task: any
  taskId: String
  constructor(
    private apollo: Apollo,
    public route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params.subscribe(p => {
      this.taskId = p['taskId'];
    })
    this.refresh();
  }

  refresh(){

    console.log(`
    query{
      task(taskId:"${this.taskId}"){
        id
        name
        description
        startDate
        deadline
        status
        state
        taskCode
        createdDate
        parent{
          taskCode
        }
      }
    }
    `)

    this.taskSub = this.apollo.subscribe<any>({
      query: gql`
      subscription{
        task(taskId:"${this.taskId}"){
          id
          name
          description
          startDate
          deadline
          status
          state
          taskCode
          createdDate
          parent{
            id
            name
            taskCode
            project{
              id
              name
              projectCode
            }
          }
        }
      }
      `
    })
      .pipe(
        map(result => result.data['task'])
      )
      this.taskSub.subscribe(data => {
        this.task = data
        this.taskCodeForSubTask = data.taskCode
        console.log("taskCodeForSubTask",this.taskCodeForSubTask)
      })
    }

}


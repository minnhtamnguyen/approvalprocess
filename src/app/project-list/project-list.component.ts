import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  projects: Observable<any[]>;
  constructor(private apollo: Apollo) { }
  ngOnInit() {
    this.projects = this.apollo.watchQuery<any>({
      query: gql`
      query{
        allProjects{
          id
          name
          description
          startDate
          status
          approve
          createdDate
          projectCode
        }
      }
      `
    })
      .valueChanges
      .pipe(
        map(result => result.data['allProjects'])
      );
  }

}

import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  tasks: Observable<any[]>;
  constructor(private apollo: Apollo) { }
  ngOnInit() {
    this.tasks = this.apollo.watchQuery<any>({
      query: gql`
      query{
        allTasks{
          id
          name
          description
          startDate
          deadline
          status
          approve
          taskCode
          createdDate
        }
      }
      `
    })
      .valueChanges
      .pipe(
        map(result => result.data['allTasks'])
      );
  }


}

import { Component, OnInit, Input } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {

  @Input()
  projectCodeForTask
  
  task: Observable<any>

  constructor(
    private apollo: Apollo,
  ) { }

  formdata: FormGroup;

  ngOnInit() {

    this.formdata = new FormGroup({
      taskName: new FormControl("", Validators.required),
      taskCode: new FormControl("", Validators.required),
      taskDesc: new FormControl("", Validators.required),
      taskStart: new FormControl("", Validators.required),
      taskDeadline: new FormControl("", Validators.required),
    });
  }

  createTask() {
    var inputData = {
      name: this.formdata.value["taskName"],
      taskCode: this.formdata.value["taskCode"],
      description: this.formdata.value["taskDesc"],
      projectCode: this.projectCodeForTask,
      startDate: this.formdata.value["taskStart"],
      deadline: this.formdata.value["taskDeadline"],
    }

    console.log(`
    mutation{
      createTask(task :{
        name:   ${JSON.stringify(inputData.name)}
        taskCode:  ${JSON.stringify(inputData.taskCode)}
        description:  ${JSON.stringify(inputData.description)}
        projectCode:  ${JSON.stringify(this.projectCodeForTask)}
        startDate:  ${JSON.stringify(inputData.startDate)}
        deadline:  ${JSON.stringify(inputData.deadline)}
      }){
        name
        description
        taskCode
        startDate
        deadline
        project{
          projectCode
        }
      }}
    ` )

    this.apollo.mutate({
      mutation: gql`
      mutation{
        createTask(task :{
          name:   ${JSON.stringify(inputData.name)}
          taskCode:  ${JSON.stringify(inputData.name)}
          description:  ${JSON.stringify(inputData.name)}
          projectCode:  ${JSON.stringify(this.projectCodeForTask)}
          startDate:  ${JSON.stringify(inputData.startDate)}
          deadline:  ${JSON.stringify(inputData.deadline)}
          assignTo: "asa"
        }){
          name
          description
          taskCode
          startDate
          deadline
          project{
            projectCode
          }
        }}
      `
    }).subscribe((data: any) => {
      this.task = data
      console.log(this.task)
      this.formdata.reset()
    }, (error) => {
      console.log("sai")
    })
  }

}

import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService implements OnInit{

  constructor(public http: HttpClient) {
    this.process()
   }
  ngOnInit(): void {
    
  }
  curentUser: any
  
  getUser(){
    return this.http.get("user/me")
  }

  process(): void {
    this.http.get("user/me").subscribe(result => {
      this.curentUser = result;
      console.log(this.curentUser)
     // console.log("tokenValue",this.curentUser.details.tokenValue)
      localStorage.setItem("tokenType", this.curentUser.details.tokenType)
      localStorage.setItem("tokenValue", this.curentUser.details.tokenValue)
      
    })
  }

  dateCalculate(deadline,today){
    
  }



}

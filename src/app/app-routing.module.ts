import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { TaskListComponent } from './task-list/task-list.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { MemberListComponent } from './member-list/member-list.component';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { SubTaskDetailComponent } from './sub-task-detail/sub-task-detail.component';
import { ReleaseListComponent } from './release-list/release-list.component';
import { ProjectComponent } from './project/project/project.component';
import { ProjectDetailComponent } from './project/project-detail/project-detail.component';
import { TasksComponent } from './project/tasks/tasks.component';
import { MembersComponent } from './project/members/members.component';
import { ActivitesComponent } from './project/activites/activites.component';
import { ReportsComponent } from './project/reports/reports.component';
import { ReleasesComponent } from './project/releases/releases.component';
import { TaskComponent } from './task/task/task.component';
import { TaskDetailComponent } from './task/task-detail/task-detail.component';

const routes: Routes = [
  {path:"projects", component: ProjectListComponent},
  {path:"", component: HomeComponent},
  {path:"header", component: HeaderComponent},
  {path:"projectDetail/:projectId/tasks", component: TaskListComponent},
  {path:"createProject", component: CreateProjectComponent},
  {path:"createTask", component: CreateTaskComponent},
  {path:"projectDetail/:projectId/members", component: MemberListComponent},
  {path:"projectDetail/:projectId/activities", component: ActivityListComponent},
  {path:"subTaskDetail/:taskId", component: SubTaskDetailComponent},
  {path:"projectDetail/:projectId/releaseList", component: ReleaseListComponent},
  {path: "projects/:projectId", component: ProjectComponent, children:[
    {
      path: "", component: ProjectDetailComponent
    },
    {
      path: "tasks", component: TasksComponent
    },
    {
      path: "members", component: MembersComponent
    },
    {
      path: "activities", component: ActivitesComponent
    },
    {
      path: "reports", component: ReportsComponent
    },
    {
      path: "releases", component: ReleasesComponent
    }
  ]},
  {path: "projects/:projectId/tasks/:taskId", component: TaskComponent, children:[
    {
      path: "", component: TaskDetailComponent
    },
    {
      path: "tasks", component: TasksComponent
    },
    {
      path: "members", component: MembersComponent
    },
    {
      path: "activities", component: ActivitesComponent
    },
    {
      path: "reports", component: ReportsComponent
    },
    {
      path: "releases", component: ReleasesComponent
    }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

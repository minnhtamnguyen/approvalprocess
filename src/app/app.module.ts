import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Apollo, ApolloModule } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { AppRoutingModule } from './app-routing.module';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { AppComponent } from './app.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectInfoComponent } from './project-info/project-info.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TaskListComponent } from './task-list/task-list.component';
import { MemberListComponent } from './member-list/member-list.component';
import { MemberDetailComponent } from './member-detail/member-detail.component';
import { MomentModule } from 'ngx-moment';
import { CreateProjectComponent } from './create-project/create-project.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { SubTaskDetailComponent } from './sub-task-detail/sub-task-detail.component';
import { CreateSubtaskComponent } from './create-subtask/create-subtask.component';
import { ReleaseListComponent } from './release-list/release-list.component';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { split } from 'apollo-link';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { ProjectModule } from './project/project.module';
import { TaskModule } from './task/task.module';

@NgModule({
  declarations: [
    AppComponent,
    ProjectListComponent,
    ProjectInfoComponent,
    HomeComponent,
    HeaderComponent,
    SidebarComponent,
    TaskListComponent,
    MemberListComponent,
    MemberDetailComponent,
    CreateProjectComponent,
    CreateTaskComponent,
    ActivityListComponent,
    SubTaskDetailComponent,
    CreateSubtaskComponent,
    ReleaseListComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    ApolloModule,
    HttpLinkModule,
    MomentModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ProjectModule,
    TaskModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [CreateProjectComponent]
})
export class AppModule {
  constructor(
    apollo: Apollo,
    httpLink: HttpLink) {
      const ws = new WebSocketLink({
        uri: `ws://localhost:8083/subscriptions`,
        options: {
          reconnect: true
        }
      });
      const http = httpLink.create({ 
        uri: '/graphql',
        withCredentials: true,
        headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('tokenValue')}`)
      });
      const link = split(({ query }) => {
        const { kind, operation }: any = getMainDefinition(query);
        return kind === 'OperationDefinition' && operation === 'subscription';
      }, ws, http);
      apollo.create({
        link: link,
        cache: new InMemoryCache(),
        defaultOptions: {
          query: {
            fetchPolicy: 'network-only'
          }
        }
      });
  }
}
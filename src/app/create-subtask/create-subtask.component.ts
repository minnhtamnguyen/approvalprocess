import { Component, OnInit, Input } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-create-subtask',
  templateUrl: './create-subtask.component.html',
  styleUrls: ['./create-subtask.component.css']
})
export class CreateSubtaskComponent implements OnInit {

  @Input()
  taskCodeForSubTask
  subTask: Observable<any>

  constructor(
    private apollo: Apollo,
  ) { }

  formdata: FormGroup;

  ngOnInit() {

    this.formdata = new FormGroup({
      subTaskName: new FormControl("", Validators.required),
      subTaskCode: new FormControl("", Validators.required),
      subTaskDesc: new FormControl("", Validators.required),
      subTaskStart: new FormControl("", Validators.required),
      subTaskDeadline: new FormControl("", Validators.required),
    });
  }

  createSubTask() {
    var inputData = {
      name: this.formdata.value["subTaskName"],
      taskCode: this.formdata.value["subTaskCode"],
      description: this.formdata.value["subTaskDesc"],
      projectCode: this.taskCodeForSubTask,
      startDate: this.formdata.value["subTaskStart"],
      deadline: this.formdata.value["subTaskDeadline"],
    }

    console.log(`
    mutation{
      createTask(task :{
        name:   ${JSON.stringify(inputData.name)}
        taskCode:  ${JSON.stringify(inputData.name)}
        description:  ${JSON.stringify(inputData.name)}
        parentCode:  ${JSON.stringify(this.taskCodeForSubTask)}
        startDate:  ${JSON.stringify(inputData.startDate)}
        deadline:  ${JSON.stringify(inputData.deadline)}
      }){
        name
        description
        taskCode
        startDate
        deadline
        project{
          projectCode
        }
        parent{
      		taskCode
    		}
      }}
    ` )

    this.apollo.mutate({
      mutation: gql`
      mutation{
        createTask(task :{
          name:   ${JSON.stringify(inputData.name)}
          taskCode:  ${JSON.stringify(inputData.name)}
          description:  ${JSON.stringify(inputData.name)}
          parentCode:  ${JSON.stringify(this.taskCodeForSubTask)}
          startDate:  ${JSON.stringify(inputData.startDate)}
          deadline:  ${JSON.stringify(inputData.deadline)}
          assignTo: "asa"
        }){
          name
          description
          taskCode
          startDate
          deadline
          project{
            projectCode
          }
          parent{
            taskCode
          }
        }}
      `
    }).subscribe((data: any) => {
      this.subTask = data
      console.log(this.subTask)
    }, (error) => {
      console.log("sai")
    })
  }
}

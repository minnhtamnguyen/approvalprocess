import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {

  activities: any

  constructor(
    public apollo: Apollo
  ) { }

  ngOnInit() {

    this.activities = this.apollo.subscribe<any>({
      query: gql`
      query{
        allActivities{
          id
          description
          function
          createdDate
        }
      }
      `
    })
      .pipe(
        map(result =>  result.data['allActivities'])
      );
  }

}

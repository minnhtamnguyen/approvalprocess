import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.css']
})
export class ProjectInfoComponent implements OnInit {

  project: Observable<any[]>;
  projectId: String
  constructor(
    private apollo: Apollo,
    public route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params.subscribe(p => {
      this.projectId = p['projectId'];})

    this.project = this.apollo.watchQuery<any>({
      query: gql`
      query{
        project(projectId:"${this.projectId}"){
          id
          name
  description
  startDate
  status
  approve
  createdDate
  projectCode
  createdBy
        }
      }
      `
    })
      .valueChanges
      .pipe(
        map(result => result.data['project'])
      );
  }
}

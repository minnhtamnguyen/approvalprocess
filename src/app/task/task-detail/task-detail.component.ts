import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { ActivatedRoute } from '@angular/router';
import { UserServiceService } from 'src/app/user-service.service';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {

  taskSuscriber: Observable<any>;
  task: any;
  taskId: String;
  currentUser: any;

  createdDateView: FormControl = new FormControl({ value: '', disabled: true })
  startDateView: FormControl = new FormControl({ value: '', disabled: true })
  deadlineView: FormControl = new FormControl({ value: '', disabled: true })
  displayedTaskColumns: string[] = ['name', 'taskCode', 'startDate'];

  constructor(
    private apollo: Apollo,
    public route: ActivatedRoute,
    public userService: UserServiceService) { }
  ngOnInit() {
    this.route.params.subscribe(p => {
      this.taskId = p['taskId'];
    })
    this.refresh();
    this.userService.getUser().subscribe(user => {
      this.currentUser = user
    })
  }

  hasRole(role) {
    for (let i = 0; i < this.currentUser.authorities.length; i++) {
      if (this.currentUser.authorities[i].authority === role) {
        return true
      }
    }
    return false
  }

  refresh() {
    this.taskSuscriber = this.apollo.subscribe<any>({
      query: gql`
      subscription{
        task(taskId:"${this.taskId}"){
          id
          name
          description
          startDate
          deadline
          status
          state
          taskCode
          createdDate
          project{
            id
            name
            projectCode
          }
          subTasks{
            id
            name
            startDate
            taskCode
          }
        }
      }
      `
    })
      .pipe(
        map(result => result.data['task'])
      )
    this.taskSuscriber.subscribe(data => {
      this.task = data
      this.createdDateView.setValue((new Date(data.createdDate)).toISOString())
      this.startDateView.setValue((new Date(data.startDate)).toISOString())
      this.deadlineView.setValue((new Date(data.deadline)).toISOString())
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-menu',
  templateUrl: './task-menu.component.html',
  styleUrls: ['./task-menu.component.css']
})
export class TaskMenuComponent implements OnInit {

  projectId: String
  constructor(public route: ActivatedRoute) { }
  
  ngOnInit() {
    this.route.params.subscribe(p => {
      this.projectId = p['projectId'];
    })
  }

}

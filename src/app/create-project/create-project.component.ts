import { Component, OnInit, Input, Inject } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import gql from 'graphql-tag';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent {
  project: Observable<any>
  constructor(
    private apollo: Apollo,
    public dialogRef: MatDialogRef<CreateProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  formdata = new FormGroup({
    projectName: new FormControl("", Validators.required),
    projectCode: new FormControl("", Validators.required),
    projectDesc: new FormControl("", Validators.required),
    projectStart: new FormControl("", Validators.required),
    projectDeadline: new FormControl("", Validators.required),
  });
  onNoClick(): void {
    this.dialogRef.close();
  }
  createProject() {
    var inputData = {
      name: this.formdata.value["projectName"],
      projectCode: this.formdata.value["projectCode"],
      description: this.formdata.value["projectDesc"],
      startDate: this.formdata.value["projectStart"],
      deadline: this.formdata.value["projectDeadline"],
    }
    this.apollo.mutate({
      mutation: gql`
    mutation{
      createProject(project : {
        name: ${JSON.stringify(inputData.name)},
        projectCode: ${JSON.stringify(inputData.projectCode)},
        description: ${JSON.stringify(inputData.description)},
        startDate: ${JSON.stringify(inputData.startDate)},
        deadline: ${JSON.stringify(inputData.deadline)}
      }){
        name
        projectCode
        description
        startDate
        deadline
      }
    }
    `
    }).subscribe((data: any) => {
      this.project = data
      this.onNoClick()
    }, (error) => {
    })
  }
}

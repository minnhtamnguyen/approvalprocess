import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public http: HttpClient) { }

  ngOnInit() {
  }

  logout(){
    this.http.get("logout").subscribe()
    
    this.http.get("auth/logout").subscribe(()=>{
      this.http.get("logout").subscribe()
      window.location.reload();
    })
  }

}

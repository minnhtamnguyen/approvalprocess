import { Component, OnInit, Inject } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import gql from 'graphql-tag';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {

  projectCode: string;
  assignable: Array<any>;
  constructor(
    private apollo: Apollo,
    public dialogRef: MatDialogRef<CreateTaskComponent>,
    public toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.projectCode = data.projectCode;
    this.assignable = data.assignable
  }

  formdata: FormGroup;

  ngOnInit() {

    this.formdata = new FormGroup({
      taskName: new FormControl("", Validators.required),
      taskCode: new FormControl("", Validators.required),
      taskDesc: new FormControl("", Validators.required),
      taskStart: new FormControl("", Validators.required),
      taskDeadline: new FormControl("", Validators.required),
    });
  }

  createTask() {
    var inputData = {
      name: this.formdata.value["taskName"],
      taskCode: this.formdata.value["taskCode"],
      description: this.formdata.value["taskDesc"],
      projectCode: this.projectCode,
      startDate: this.formdata.value["taskStart"],
      deadline: this.formdata.value["taskDeadline"],
    }
    this.apollo.mutate({
      mutation: gql`
      mutation{
        createTask(task :{
          name:   ${JSON.stringify(inputData.name)}
          taskCode:  ${JSON.stringify(inputData.taskCode)}
          description:  ${JSON.stringify(inputData.description)}
          projectCode:  ${JSON.stringify(this.projectCode)}
          startDate:  ${JSON.stringify(inputData.startDate)}
          deadline:  ${JSON.stringify(inputData.deadline)}
          assignTo: "asa"
        }){
          name
          description
          taskCode
          startDate
          deadline
          project{
            projectCode
          }
        }}
      `
    }).subscribe((data: any) => {
      this.toastr.success("Task is created!", "Created")
      this.onNoClick();
    }, (error) => {
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

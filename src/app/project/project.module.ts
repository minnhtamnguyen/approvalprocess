import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRoutingModule } from './project-routing.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectComponent } from './project/project.component';
import { MaterialModule } from '../material-module';
import { ProjectMenuComponent } from './project-menu/project-menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TasksComponent } from './tasks/tasks.component';
import { MembersComponent } from './members/members.component';
import { ActivitesComponent } from './activites/activites.component';
import { ReportsComponent } from './reports/reports.component';
import { ReleasesComponent } from './releases/releases.component';
import { BoardComponent } from './board/board.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { MomentModule } from 'ngx-moment';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CreateUserComponent } from './create-user/create-user.component';

@NgModule({
  declarations: [ProjectDetailComponent, ProjectComponent, ProjectMenuComponent, TasksComponent, MembersComponent, ActivitesComponent, ReportsComponent, ReleasesComponent, BoardComponent, CreateTaskComponent, CreateUserComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxChartsModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        'm': 59
      }
    })
  ],
  exports: [ProjectDetailComponent, ProjectComponent, TasksComponent, MembersComponent, ActivitesComponent, ReportsComponent, ReleasesComponent]
})
export class ProjectModule { }

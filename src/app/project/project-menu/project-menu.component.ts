import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-menu',
  templateUrl: './project-menu.component.html',
  styleUrls: ['./project-menu.component.css']
})
export class ProjectMenuComponent implements OnInit {

  projectId: String
  constructor(public route: ActivatedRoute) { }
  
  ngOnInit() {
    this.route.params.subscribe(p => {
      this.projectId = p['projectId'];
    })
  }

}

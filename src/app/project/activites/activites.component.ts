import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Component({
  selector: 'app-activites',
  templateUrl: './activites.component.html',
  styleUrls: ['./activites.component.css']
})
export class ActivitesComponent implements OnInit {

  activities: any

  constructor(
    public apollo: Apollo
  ) { }

  ngOnInit() {

    this.activities = this.apollo.subscribe<any>({
      query: gql`
      query{
        allActivities{
          id
          description
          function
          createdDate
        }
      }
      `
    })
      .pipe(
        map(result =>  result.data['allActivities'])
      );
  }

}

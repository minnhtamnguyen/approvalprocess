import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  projectCode: string;

  constructor(
    private apollo: Apollo,
    public dialogRef: MatDialogRef<CreateUserComponent>,
    public toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.projectCode = data.projectCode;
  }

  formdata: FormGroup;

  ngOnInit() {

    this.formdata = new FormGroup({
      username: new FormControl("", Validators.required),
      fullName: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required),
    });
  }

  createUser() {
    var inputData = {
      username: this.formdata.value["username"],
      fullName: this.formdata.value["fullName"],
      password: this.formdata.value["password"],
      projectCode: this.projectCode,
    }
    this.apollo.mutate({
      mutation: gql`
      mutation{
        createUserInfo(userInfo :{
          name:   ${JSON.stringify(inputData.fullName)}
          username:  ${JSON.stringify(inputData.username)}
          password:  ${JSON.stringify(inputData.password)}
          projectCode:  ${JSON.stringify(this.projectCode)}
        }){
          id
        }}
      `
    }).subscribe((data: any) => {
      this.toastr.success("User is created!", "Created")
      this.onNoClick();
    }, (error) => {
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

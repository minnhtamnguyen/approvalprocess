import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { ActivatedRoute } from '@angular/router';
import { UserServiceService } from 'src/app/user-service.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import gql from 'graphql-tag';
import { CreateUserComponent } from '../create-user/create-user.component';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  displayedPeopleColumns: string[] = ['name', 'username'];
  members : any[];
  projectScriber: Observable<any>;
  projectCode: String;
  constructor(
    private apollo: Apollo,
    public route: ActivatedRoute,
    public userService: UserServiceService,
    public toastr: ToastrService,
    public dialog: MatDialog
  ) { }

  projectId: String;
  ngOnInit(): void {

    if(!this.projectId){
      this.route.parent.params.subscribe(p => {
        this.projectId = p['projectId'];
    })
    this.getTasks();
    }
  }

  getTasks() {
    this.projectScriber = this.apollo.subscribe<any>({
      query: gql`
      subscription{
        project(projectId:"${this.projectId}"){
          id
          projectCode
          members{
            id
            name
            username
          }
        }
      }
      `
    })
      .pipe(
        map(result => result.data['project'])
      );
    this.projectScriber.subscribe(data => {
      this.members = data.members;
      this.projectCode = data.projectCode;
    })
  }

  createUserDialog(){
    const dialogRef = this.dialog.open(CreateUserComponent, {
      width: '30%',
      data: { projectCode: this.projectCode }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

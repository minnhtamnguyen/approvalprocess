import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ActivatedRoute } from '@angular/router';
import { UserServiceService } from 'src/app/user-service.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { CreateTaskComponent } from '../create-task/create-task.component';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  displayedTaskColumns: string[] = ['name', 'taskCode', 'startDate'];
  tasks : any[];
  members: any[]
  projectScriber: Observable<any>;
  projectCode: String;
  constructor(
    private apollo: Apollo,
    public route: ActivatedRoute,
    public userService: UserServiceService,
    public toastr: ToastrService,
    public dialog: MatDialog
  ) { }

  projectId: String;
  ngOnInit(): void {

    if(!this.projectId){
      this.route.parent.params.subscribe(p => {
        this.projectId = p['projectId'];
    })
    this.getTasks();
    }
  }

  getTasks() {
    this.projectScriber = this.apollo.subscribe<any>({
      query: gql`
      subscription{
        project(projectId:"${this.projectId}"){
          id
          projectCode
          tasks{
            id
            name
            taskCode
            startDate
          }
          members{
            id
            name
            username
          }
        }
      }
      `
    })
      .pipe(
        map(result => result.data['project'])
      );
    this.projectScriber.subscribe(data => {
      this.tasks = data.tasks;
      this.members = data.members;
      this.projectCode = data.projectCode;
    })
  }

  createTaskDialog(): void {
    const dialogRef = this.dialog.open(CreateTaskComponent, {
      width: '30%',
      data: { projectCode: this.projectCode, assignable: this.members }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

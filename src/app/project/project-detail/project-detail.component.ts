import { Component, OnInit, OnDestroy } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { map } from 'rxjs/operators';
import gql from 'graphql-tag';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserServiceService } from 'src/app/user-service.service';
import { CreateTaskComponent } from '../create-task/create-task.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

  projectScriber: Observable<any>;
  project: any
  projectId: String;
  currentUser: any;

  createdDateView: FormControl = new FormControl({ value: '', disabled: true })
  startDateView: FormControl = new FormControl({ value: '', disabled: true })
  deadlineView: FormControl = new FormControl({ value: '', disabled: true })
  displayedTaskColumns: string[] = ['name', 'taskCode', 'startDate'];
  displayedPeopleColumns: string[] = ['name', 'username'];

  constructor(
    private apollo: Apollo,
    public route: ActivatedRoute,
    public userService: UserServiceService,
    public toastr: ToastrService,
    public dialog: MatDialog
  ) { }
  ngOnDestroy(): void {
    this.projectScriber
  }

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.projectId = p['projectId'];
    });
    this.getProject();
    this.userService.getUser().subscribe(user => {
      this.currentUser = user
    })

  }
  hasRole(role) {
    for (let i = 0; i < this.currentUser.authorities.length; i++) {
      if (this.currentUser.authorities[i].authority === role) {
        return true
      }
    }
    return false
  }

  getProject() {
    this.projectScriber = this.apollo.subscribe<any>({
      query: gql`
      subscription{
        project(projectId:"${this.projectId}"){
          id
          name
          description
          startDate
          status
          state
          createdDate
          projectCode
          deadline
          createdBy
          tasks{
            id
            name
            taskCode
            startDate
          }
          members{
            id
            username
            name
          }
        }
      }
      `
    })
      .pipe(
        map(result => result.data['project'])
      );
    this.projectScriber.subscribe(data => {
      this.project = data
      this.createdDateView.setValue((new Date(data.createdDate)).toISOString())
      this.startDateView.setValue((new Date(data.startDate)).toISOString())
      this.deadlineView.setValue((new Date(data.deadline)).toISOString())
    })
  }

  approveProject() {

    console.log(`
    approveProject(id:${JSON.stringify(this.projectId)}){
      id
    }
    `)

    this.apollo.mutate({
      mutation: gql`
      mutation{
        approveProject(id:${JSON.stringify(this.projectId)}){
          id
        }
      }
      `
    }).subscribe(() => {
      this.toastr.success('This project is approved!', 'Approved!');
    })
  }

  declineProject() {
    console.log(`
    mutation{
      rejectProject(id:${JSON.stringify(this.projectId)}){
        id
      }
    }
    `)

    this.apollo.mutate({
      mutation: gql`
      mutation{
        rejectProject(id:${JSON.stringify(this.projectId)}){
          id
        }
      }
      `
    }).subscribe(() => {
      this.toastr.warning('This project is rejected!', 'Rejected!');
    })
  }
  createTaskDialog(): void {
    console.log(this.project.members)
    const dialogRef = this.dialog.open(CreateTaskComponent, {
      width: '30%',
      data: { projectCode: this.project.projectCode, assignable: this.project.members }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
